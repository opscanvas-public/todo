FROM --platform=linux/amd64 python:3.12.0-slim-bullseye

LABEL org.opencontainers.image.authors="jason@opscanvas.com"

WORKDIR /app

RUN apt update && \ 
    apt install -y curl gcc && \ 
    curl -sSL https://pdm.fming.dev/install-pdm.py | python3 - && \
    apt purge -y curl


COPY ./pdm.lock /app/
COPY ./pyproject.toml /app/
COPY ./launch.sh /app/
COPY ./src/ /app/

RUN $HOME/.local/bin/pdm install --prod --no-editable && \
    apt purge -y gcc

CMD ["/bin/bash", "-c", "/app/launch.sh"]


