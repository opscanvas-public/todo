source /app/.venv/bin/activate
cd /app/

uvicorn main:app --host 0.0.0.0 --port 8000 --root-path ${ROOT_PATH:-/}