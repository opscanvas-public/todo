Todo App
====

A simple Todo API that can be used to illustrate [OpsCanvas](https://opscanvas.com) Draw & Deploy.

# Setup

This project uses the [PDM package manager](https://pdm.fming.dev/latest/) for python, and is written using python 3.11.5. The file [.python-version](./python-version) file is for use with [pyenv](https://github.com/pyenv/pyenv), but that is not a requirement, only that you have python 3.11.5 installed.

Once the correct version of python is available, install pdm with either `pip install pdm` or follow the [installation instructions](https://pdm.fming.dev/latest/#installation).

## Configure workspace

Execute the following commands to set up a working directory for the project:

### Create a virtual environment
> ```pdm venv create```


### Add the project directory to the virtual environment's sys.path
> ```echo $PWD > .venv/lib/python3.11/site-packages/project.pth```

### Configure the python interpreter for use with this venv
> ````pdm use````

_select the interpreter located in the .venv sub-directory of the workspace root_

### Activate the virtual environment
> ```eval $(pdm venv activate)```

_this command should be run whenever you want to use the virtual environment_

_use `deactivate` to exit the virtual environment_

### Install the project dependencies
> ```pdm install```

_dependencies are installed into the `.venv/lib/python3.11/site-packages` directory_

### Verify the installation
> ```python src/main.py --version```

_prints the version of the project from the pyproject.toml file_

# Environment Variables

Configuration settings for this service

| Variable | Description | Default |
|----------|-------------|---------|
| DB_HOST  | the host address of the postgres database | localhost |
| DB_PORT | the port that the postgres database listens on | 5432 |
| DB_USER | a user with read/write access to the database | postgres |
| DB_PASSWORD | the password for the given user | postgres |
| DB_NAME | the name of the database to connect to | todo |
| ROOT_PATH | the url to this service if it is running behind a proxy | None |