import asyncio
import logging
import logging.config
import os
import re
import sys
import time
import tomllib
import uuid
from datetime import datetime, timezone

import yaml
from asyncpg import Connection, Pool
from fastapi import FastAPI, Request
from fastapi import status as statuses
from fastapi.exceptions import FastAPIError
from fastapi.middleware.cors import CORSMiddleware
from starlette.responses import JSONResponse

import dependencies
from config import RuntimeSettings
from endpoints import health, todo_item, todo_list

# Configure logging
settings = RuntimeSettings()
try:
    log_config_path = f"{os.path.dirname(os.path.realpath(__file__))}/logging.yaml"
    print(f"Reading logger config from {log_config_path}")
    with open(log_config_path, "r") as y:
        d = yaml.safe_load(y)
        logging.config.dictConfig(d)

    logging.Formatter.converter = time.gmtime
except Exception as e:
    tm = datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S")
    msg = f"An error occurred during logging - {e}"
    sys.stdout.write(f"[{tm} ERROR] {msg}")
    sys.exit(0)

logger = logging.getLogger("todo")

app = FastAPI(
    docs_url="/docs",
    redoc_url="/redoc",
    openapi_url="/openapi.json",
)

# Configure server and documentation


def get_title(s: str) -> str:
    return re.sub(r"(\w)([A-Z])", r"\1 \2", s).replace("Ops Canvas", "OpsCanvas")


try:
    with open("pyproject.toml", "rb") as f:
        data = tomllib.load(f)
        version = str(data["project"]["version"])
        get_title = get_title(str(data["project"]["name"]))
        description = str(data["project"]["description"])

    app = FastAPI(title=f"✨ {get_title} ✨",
                  description=description, version=version)

    app.add_middleware(
        CORSMiddleware,
        allow_origins=settings.ORIGINS,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    app.include_router(health.router)
    app.include_router(todo_list.router)
    app.include_router(todo_item.router)
except RuntimeError as e:
    print(e)
    sys.exit(1)


@app.on_event("startup")
async def startup_event():
    logger.info(f"Starting API service v{version}")

    def sanitize(k: str, v: str) -> str:
        scrub = "PASSWORD" in k or "key" in k
        return f"{k}: {v[:4]}xxxxx" if scrub else f"{k}: {v}"

    settings_dispay = "\n\t".join(
        [f"{sanitize(k, v)}" for k, v in settings.model_dump().items()])
    logger.info(f"Settings: \n\t{settings_dispay}")

    try:
        # initialize the database connection pool
        pool = await dependencies.get_connection_pool()  # type: ignore
        res = await pool.fetch("SELECT 1")
        if len(res) != 1:
            raise FastAPIError("Could not initialize db")

        async with pool.acquire() as conn:
            await migrate_db(conn)

        logger.info(
            f"Established database connection\n\thost: {settings.DB_HOST}\n\tport: {settings.DB_PORT}\n\tdatabase: {
                settings.DB_NAME}\n\tuser: {settings.DB_USER}\n\tpassword: {settings.DB_PASSWORD[:4]}xxxxx"
        )

    except Exception as e:
        raise FastAPIError(f"Failed to connect to the database {e}")

    logger.info("API accepting connections")


@app.on_event("shutdown")
async def shutdown_event():
    pool: Pool = await dependencies.get_connection_pool()  # type: ignore
    if pool is not None:
        logger.info("Closing the database connection pool")
        try:
            await asyncio.wait_for(pool.close(), timeout=30)
        except asyncio.TimeoutError:
            logger.warn(
                "Could not close the database connection pool within 30 seconds")

    logger.info("Shutdown complete")


@app.exception_handler(Exception)
async def uvicorn_exception_handler(request: Request, exc: Exception):
    id = uuid.uuid4()

    exc.with_traceback(None)
    logger.error(
        f"[{id}] An error occurred processing {request.method.upper()} {request.url} - {exc}", exc_info=False)

    response = JSONResponse(
        status_code=statuses.HTTP_500_INTERNAL_SERVER_ERROR,
        content={
            "message": f"An error occurred. Please contact support and provide this ID: {id}"},
    )
    # Add CORS headers to FastAPI exception_handler
    origin = request.headers.get("origin")
    if origin and origin in settings.ORIGINS:
        response.headers["Access-Control-Allow-Credentials"] = "true"
        response.headers["Access-Control-Allow-Origin"] = origin
    return response


async def migrate_db(conn: Connection):
    await conn.execute("CREATE SCHEMA IF NOT EXISTS todo")

    await conn.execute("""
        CREATE TABLE IF NOT EXISTS todo.list(
            id serial primary key,
            name text,
            complete bool default false,
            created timestamptz,
            notes text,
            completed_on timestamptz
        )
    """)

    await conn.execute("""
        CREATE TABLE IF NOT EXISTS todo.item(
            id serial primary key,
            list_id int references todo.list(id),
            label text,
            added timestamptz,
            complete bool default false,
            completed_on timestamptz
        )
    """)
