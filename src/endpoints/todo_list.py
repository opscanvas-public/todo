import logging
from datetime import datetime, timezone

from asyncpg import Connection
from fastapi import APIRouter, Body, Depends, responses
from fastapi import status as http_status
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import HTTPException

import dependencies
from endpoints.resources import TodoList

router = APIRouter(tags=["Lists"], prefix="/lists")
logger = logging.getLogger("todo")


@router.get(
    "/",
    description="Fetch all todo lists",
    responses={
        "200": {
            "description": "A list of todo lists (300 max)",
            "model": list[TodoList]
        }
    })
async def list_todo_lists(
    conn: Connection = Depends(dependencies.get_connection)
):
    res = await conn.fetch("SELECT * FROM todo.list LIMIT 300")
    lists = [TodoList(**item) for item in res]

    return responses.JSONResponse(status_code=http_status.HTTP_200_OK, content=jsonable_encoder(lists))


@router.get(
    "/{list_id}",
    response_model=TodoList,
    description="Fetch a todo list"
)
async def get_todo_list(
    list_id: int,
    conn: Connection = Depends(dependencies.get_connection)
):
    res = await conn.fetchrow(
        "SELECT * FROM todo.list WHERE id = $1",
        list_id
    )

    return responses.JSONResponse(
        status_code=http_status.HTTP_200_OK,
        content=jsonable_encoder(TodoList(**res))
    )


@router.post(
    "/",
    description="Create a new todo list",
    response_model=TodoList
)
async def create_todo_list(
    conn: Connection = Depends(dependencies.get_connection),
    name: str = Body(),
    notes: str = Body("")
):
    created = datetime.now().astimezone(timezone.utc)
    res = await conn.fetchval(
        "INSERT INTO todo.list(name, notes, created) VALUES ($1, $2, $3) RETURNING id",
        name,
        notes,
        created
    )

    todo_list = TodoList(id=int(res), name=name, notes=notes, created=created)
    return responses.JSONResponse(status_code=http_status.HTTP_200_OK, content=jsonable_encoder(todo_list.model_dump()))


@router.patch(
    "/{list_id}",
    description="Update a todo list",
    response_model=TodoList
)
async def update_todo_list(
    list_id: int,
    name: str | None = Body(None),
    notes: str | None = Body(None),
    complete: bool | None = Body(None),
    conn: Connection = Depends(dependencies.get_connection)
):
    if name is None and notes is None and complete is None:
        raise HTTPException(status_code=http_status.HTTP_400_BAD_REQUEST,
                            detail="Must specify at least one of name, notes or complete")

    completed_on = datetime.utcnow().astimezone(timezone.utc) if complete is True else None

    args = []
    if name is not None:
        args.append(name)
        nm = f"name = ${len(args)}"
    else:
        nm = "name = name"

    if notes is not None:
        args.append(notes)
        nt = f"notes = ${len(args)}"
    else:
        nt = "notes = notes"

    co = "completed_on = completed_on"
    if complete is not None:
        args.append(complete)
        ce = f"complete = ${len(args)}"

        if complete is True:
            args.append(completed_on)
            co = f"completed_on = ${len(args)}"
        else:
            co = "completed_on = null"
    else:
        ce = "complete = complete"

    args.append(list_id)
    await conn.execute(f"""
        UPDATE todo.list
        SET {nm},
            {nt},
            {ce},
            {co}
        WHERE id = ${len(args)}
    """, *args)

    return await get_todo_list(list_id, conn)


@router.delete(
    "/{list_id}",
    response_model=None,
    description="Delete a todo list"
)
async def delete_todo_list(
    list_id: int,
    conn: Connection = Depends(dependencies.get_connection)
):
    await conn.execute("DELETE FROM todo.item WHERE list_id = $1", list_id)
    await conn.execute("DELETE FROM todo.list WHERE id = $1", list_id)

    return responses.JSONResponse(status_code=http_status.HTTP_200_OK, content={})
