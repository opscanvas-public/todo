
import logging

from asyncpg import Pool
from fastapi import APIRouter, Depends
from fastapi import status as http_status
from fastapi.responses import JSONResponse

import dependencies

router = APIRouter(tags=["Health"], prefix="/_", )
logger = logging.getLogger("todo")


@router.get("/ready", description="The API is ready to accept connections")
async def is_ready(conn: Pool = Depends(dependencies.get_connection_pool)):
    try:
        await conn.execute("SELECT 1")
        return JSONResponse(status_code=http_status.HTTP_200_OK, content={})
    except Exception:
        return JSONResponse(status_code=http_status.HTTP_502_BAD_GATEWAY, content={})


@router.get("/live", description="The API is healthy")
async def is_live():
    return JSONResponse(status_code=http_status.HTTP_200_OK, content={})


@router.get("/start", description="The API has started")
async def has_started(conn: Pool = Depends(dependencies.get_connection_pool)):
    return await is_ready(conn)
