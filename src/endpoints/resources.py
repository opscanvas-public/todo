from datetime import datetime

from pydantic import BaseModel


class TodoList(BaseModel):
    id: int
    name: str
    complete: bool = False
    created: datetime
    notes: str
    completed_on: datetime | None = None


class TodoItem(BaseModel):
    id: int
    list_id: int
    label: str
    complete: bool = False
    added: datetime
    completed_on: datetime | None = None
