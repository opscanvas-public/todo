import logging
from datetime import datetime, timezone

from asyncpg import Connection
from fastapi import APIRouter, Body, Depends, Query, responses
from fastapi import status as http_status
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import HTTPException

import dependencies
from endpoints.resources import TodoItem

router = APIRouter(tags=["Items"], prefix="/items")
logger = logging.getLogger("todo")


@router.get(
    "/",
    description="Get items for a list",
    response_model=list[TodoItem]
)
async def list_items(
    list_id: int = Query(),
    conn: Connection = Depends(dependencies.get_connection)
):
    res = await conn.fetch(
        "SELECT * FROM todo.item WHERE list_id = $1",
        list_id
    )
    items = [TodoItem(**dict(item)) for item in res]
    return responses.JSONResponse(status_code=http_status.HTTP_200_OK, content=jsonable_encoder(items))


@router.get(
    "/{item_id}",
    description="Get a single todo item",
    response_model=TodoItem
)
async def get_item(
    item_id: int,
    conn: Connection = Depends(dependencies.get_connection)
):
    res = await conn.fetchrow(
        "SELECT * FROM todo.item WHERE id = $1",
        item_id
    )
    return responses.JSONResponse(
        status_code=http_status.HTTP_200_OK,
        content=jsonable_encoder(TodoItem(**dict(res)))
    )


@router.post(
    "/",
    description="Add item to list",
    response_model=TodoItem
)
async def add_item(
    list_id: int = Body(),
    label: str = Body(),
    complete: bool = Body(default=False),
    conn: Connection = Depends(dependencies.get_connection)
):
    added_on = datetime.utcnow().astimezone(timezone.utc)
    completed_on = None if not complete else datetime.utcnow().astimezone(timezone.utc)

    res = await conn.fetchval(
        """
        INSERT INTO todo.item (list_id, label, complete, added, completed_on)
        VALUES ($1, $2, $3, $4, $5)
        RETURNING id
        """,
        list_id,
        label,
        complete,
        added_on,
        completed_on
    )

    item = TodoItem(
        id=int(res),
        list_id=list_id,
        label=label,
        complete=complete,
        added=added_on,
        completed_on=completed_on
    )

    return responses.JSONResponse(
        status_code=http_status.HTTP_200_OK,
        content=jsonable_encoder(item.model_dump())
    )


@router.patch(
    "/{item_id}",
    description="Update a todo item",
    response_model=TodoItem
)
async def update_item(
    item_id: int,
    label: str | None = Body(default=None),
    complete: bool | None = Body(default=None),
    conn: Connection = Depends(dependencies.get_connection)
):
    if not label and complete is None:
        raise HTTPException(status_code=http_status.HTTP_400_BAD_REQUEST,
                            detail="Must specify at least one of label or complete")

    completed_on = datetime.utcnow().astimezone(timezone.utc) if complete is True else None

    args = []
    if label:
        args.append(label)
        lb = f"label = ${len(args)}"
    else:
        lb = "label = label"

    co = "completed_on = completed_on"
    if complete is not None:
        args.append(complete)
        ce = f"complete = ${len(args)}"

        if complete is True:
            args.append(completed_on)
            co = f"completed_on = ${len(args)}"
        else:
            co = "completed_on = null"
    else:
        ce = "complete = complete"

    args.append(item_id)
    await conn.execute(f"""
        UPDATE todo.item
        SET {lb},
            {ce},
            {co}
        WHERE id = ${len(args)}
    """, *args)

    return await get_item(item_id, conn)


@router.delete(
    "/{item_id}",
    response_model=None,
    description="Delete a todo item"
)
async def delete_todo_list(
    item_id: int,
    conn: Connection = Depends(dependencies.get_connection)
):
    await conn.execute("DELETE FROM todo.item WHERE id = $1", item_id)

    return responses.JSONResponse(status_code=http_status.HTTP_200_OK, content={})
