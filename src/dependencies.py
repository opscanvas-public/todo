import json
import logging
import uuid
from functools import lru_cache
from typing import AsyncGenerator

import async_lru
import asyncpg
from asyncpg import Connection, Pool
from fastapi import Request, responses
from fastapi.exceptions import FastAPIError

from config import RuntimeSettings

logger = logging.getLogger("environment-api")


@lru_cache()
def get_settings() -> RuntimeSettings:
    return RuntimeSettings()


@async_lru.alru_cache()
async def get_connection_pool_from_url(url: str) -> Pool:
    async def set_encoders(conn: Connection):
        for tpe in ["json", "jsonb"]:
            await conn.set_type_codec(
                tpe, encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )
        return conn

    try:
        match await asyncpg.create_pool(
            dsn=url,
            min_size=10,
            max_size=100,
            setup=set_encoders,
            # the following setting was found here
            #   https://github.com/MagicStack/asyncpg/issues/530#issuecomment-751824400
            # queries were excruciatingly slow without this,
            # setting this to "off" increased performance by ~100x
            server_settings={"jit": "off"},
        ):
            case None:
                raise RuntimeError("Failed to connect to the database")

            case p:
                return p
    except Exception as e:
        raise FastAPIError(f"Failed to connect to the database {e}")


async def get_connection_pool() -> Pool:
    settings = get_settings()
    psql_url = (
        f"postgres://{settings.DB_USER}:{settings.DB_PASSWORD}"
        f"@{settings.DB_HOST}:{settings.DB_PORT}/{settings.DB_NAME}"
    )
    return await get_connection_pool_from_url(psql_url)  # type: ignore


async def _get_connection() -> asyncpg.Connection:
    """A helper method for acquiring a connection from the connection pool.
    NOTE - This connection must be closed manually."""
    pool = await get_connection_pool()  # type: ignore
    conn = await pool.acquire()

    return conn


async def get_connection() -> AsyncGenerator[asyncpg.Connection, None]:  # type: ignore
    """Get a connection from the connection pool.
    This connection is not enrolled in a transaction,
    and will be closed when the calling function exits."""
    conn = await _get_connection()
    try:
        yield conn
    finally:
        await (await get_connection_pool()).release(conn)  # type: ignore


def exception_handler(request: Request, exc: Exception):
    id = uuid.uuid4()

    message = f"An unexpected exception occurred processing {request.method.upper()} {request.url}"
    logger.error(f"{id}: {message}", exc)
    response = responses.JSONResponse(
        {"detail": {
            "id": str(id), "msg": f"An error occurred and has been logged with id {id}"}},
        status_code=500,
    )

    # Add CORS headers to FastAPI exception_handler
    origin = request.headers.get("origin")
    settings = get_settings()
    if origin and origin in settings.ORIGINS:
        response.headers["Access-Control-Allow-Credentials"] = "true"
        response.headers["Access-Control-Allow-Origin"] = origin
    return response
