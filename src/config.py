from pydantic_settings import BaseSettings


class RuntimeSettings(BaseSettings):
    DB_HOST: str = "localhost"
    DB_NAME: str = "todo"
    DB_USER: str = "postgres"
    DB_PASSWORD: str = "postgres"
    DB_PORT: int = 5432
    ORIGINS: set[str] = ["http://localhost", "http://host.docker.internal"]
